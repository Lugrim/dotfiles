#!/bin/sh
xrandr --setprovideroutputsource modesetting NVIDIA-0

export MONITORS=($(xrandr | grep ' connected' | cut -d' ' -f1))

SCRIPT_NAME="$0"

debug () {
	echo "[$SCRIPT_NAME] $1"
}

##################
### HEURISTICS ###
##################
if [ "${MONITORS[0]}" == "eDP-1-1" ]; then
	debug "Maybe an nvidia office setup?"
	if [ "${MONITORS[1]}" == "DP-1-1-1" ] \
		&& [ "${MONITORS[2]}" == "DP-1-1-2" ]
	then
		debug "Usual office 3-monitors setup"
		xrandr --output "eDP-1-1" --primary --mode "1920x1080" \
			--output "DP-1-1-1" --right-of "eDP-1-1" --mode "1920x1080" \
			--output "DP-1-1-2" --right-of "DP-1-1-1" --mode "1920x1080"
	fi
elif [ "${MONITORS[0]}" == "eDP-1" ]; then
	debug "Maybe an non-nvidia office setup?"
	if [ "${MONITORS[1]}" == "DP-1-1" ] \
		&& [ "${MONITORS[2]}" == "DP-1-2" ]
	then
		debug "Usual office 3-monitors"
		xrandr --output "eDP-1" --primary --mode "1920x1080" \
			--output "DP-1-1" --right-of "eDP-1" --mode "2560x1440" \
			--output "DP-1-2" --right-of "DP-1-1" --mode "1920x1200"
	fi
elif [ "${MONITORS[0]}" == "DVI-D-0" ] \
		&& [ "${MONITORS[1]}" == "HDMI-0" ]; then
	debug "Probably home setup with 2 monitors (indexed 0)"
	xrandr --output "HDMI-0" --primary --mode "1920x1080" \
		--output "DVI-D-0" --right-of "HDMI-0" --mode "1920x1080"
elif [ "${MONITORS[0]}" == "DVI-D-1" ] \
		&& [ "${MONITORS[1]}" == "HDMI-1" ]; then
	debug "Probably home setup with 2 monitors (indexed 1)"
	xrandr --output "HDMI-1" --primary --mode "1920x1080" \
		--output "DVI-D-1" --right-of "HDMI-1" --mode "1920x1080"
else
	debug "Setup not implemented, falling back to xrandr --auto"
	xrandr --auto
fi
