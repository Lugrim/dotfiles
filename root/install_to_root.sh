#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cp "$SCRIPT_DIR/display_setup.sh" "/etc/lightdm/display_setup.sh"
cp "$SCRIPT_DIR/lightdm.conf" "/etc/lightdm/lightdm.conf"
# ln -sf "$SCRIPT_DIR/lightdm-webkit2-greeter.conf" "/etc/lightdm/lightdm-webkit2-greeter.conf"
# ln -sf "$SCRIPT_DIR/web-greeter.yml" "/etc/lightdm/web-greeter.yml"
cp "$SCRIPT_DIR/web-greeter.yml" "/etc/lightdm/web-greeter.yml"

# PIPEWIRE SETUP #
mkdir -p /etc/pipewire/pipewire.conf.d
cp "$SCRIPT_DIR/pipewire.conf.d/*" /etc/pipewire/pipewire.conf.d
