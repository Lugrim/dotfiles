"Specify a directory for plugins
"- For Neovim: stdpath('data') . '/plugged'
"- Avoid using standard Vim directory names like 'plugin'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
"""""""""""""""""""""""""""""""
"  LIBRARIES & DEPENDENCIES   "
"""""""""""""""""""""""""""""""
"General purpose fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

"Some useful lua functions
"Required by telescope
Plug 'nvim-lua/plenary.nvim'

"Fixes / adds more icons
"Optional for Telescope
Plug 'kyazdani42/nvim-web-devicons'

"Pretty display of telescope results
"Optional for Telescope
Plug 'folke/trouble.nvim'

"""""""""""""""""""""""""""""""
"      GENERAL UTILITIES      "
"""""""""""""""""""""""""""""""
"Auto close ( [ {
"Plug 'jiangmiao/auto-pairs'

"Extends modelines with let
Plug 'vim-scripts/let-modeline.vim'

"""""""""""""""""""""""""""""""
"      GENERIC COMMANDS       "
"""""""""""""""""""""""""""""""
"Align around some characters
Plug 'junegunn/vim-easy-align'

" Simple comment
Plug 'numToStr/Comment.nvim'

"Asynchronous make
" Plug 'neomake/neomake'

"Handling & searching through Unicode characters
Plug 'chrisbra/unicode.vim'

"ALlows to add surrounding characters to selection
Plug 'tpope/vim-surround'

"Grammar checker
"Plug 'rhysd/vim-grammarous'
"Plug 'dpelle/vim-LanguageTool'

"Table management ; could be handled by vim-easy-align
"Plug 'godlygeek/tabular'

" Code formatter
Plug 'google/vim-maktaba'
Plug 'google/vim-codefmt'
Plug 'google/vim-glaive'

"""""""""""""""""""""""""""""""
"       INTERFACE STUFF       "
"""""""""""""""""""""""""""""""
"Insert code Snippets
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

"" File explorer
"Plug 'preservim/nerdtree'
"Plug 'Xuyuanp/nerdtree-git-plugin'
"Plug 'ryanoasis/vim-devicons'

"Autocomplete
Plug 'Shougo/ddc.vim'

"Nice looking fuzzy finder
"Optional dependency of todo-comments
Plug 'nvim-telescope/telescope.nvim'

"Highlights todos etc. in comments, list of todos, ...
Plug 'folke/todo-comments.nvim'

" Rainbow parentheses
Plug 'frazrepo/vim-rainbow'

" English LSP
Plug 'dense-analysis/ale'

"""""""""""""""""""""""""""""""
"     LANGUAGES SPECIFIC      "
"""""""""""""""""""""""""""""""

" COC server
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'} 

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" C
Plug 'clangd/coc-clangd'

"PYTHON
Plug 'vim-python/python-syntax'
Plug 'heavenshell/vim-pydocstring', { 'do': 'make install', 'for': 'python' }
Plug 'nvie/vim-flake8'

"MARKDOWN
"Plug 'preservim/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

"LATEX
Plug 'lervag/vimtex'
Plug 'engeljh/vim-latexfmt'

"WEB / JAVASCRIPT
"Auto complete, prettier and tslinting

"Add highlighting and indenting for JSX and TSX files
Plug 'yuezk/vim-js'
"Plug 'HerringtonDarkholme/yats.vim'
"Plug 'maxmellon/vim-jsx-pretty'
Plug 'maksimr/vim-jsbeautify'

Plug 'evanleck/vim-svelte', {'branch': 'main'}

" Scheme, LISP, ...
Plug 'Olical/conjure'

" Heredoc syntax
" Plug 'acarapetis/vim-sh-heredoc-highlighting'

"Initialize plugin system
call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""
"      GENERIC COMMANDS       "
"""""""""""""""""""""""""""""""
"Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

"Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"Snippets commands
" let g:UltiSnipsSnippetDirectories=[$HOME.'/.config/nvim/snippets']

let g:UltiSnipsExpandTrigger="<Tab>"
let g:UltiSnipsJumpForwardTrigger="<Tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-Tab>"

"""""""""""""""""""""""""""""""
"        BINARY FILES         "
"""""""""""""""""""""""""""""""
autocmd BufRead *.exe,*.bin :% !xxd
autocmd BufWritePre *.exe,*.bin :% !xxd -r
autocmd BufWritePost *.exe,*.bin :% !xxd


"""""""""""""""""""""""""""""""
"       INTERFACE STUFF       "
"""""""""""""""""""""""""""""""
:lua require("trouble").setup { }

:lua require("Comment").setup()

lua << LUA
  require("todo-comments").setup 
{
  signs = true, -- show icons in the signs column
  sign_priority = 8, -- sign priority
  -- keywords recognized as todo comments
  keywords = {
    FIX  = { icon = " ", color = "error", alt = { "FIXME", "BUG", "FIXIT", "ISSUE" } },
    TODO = { icon = " ", color = "info" },
    HACK = { icon = " ", color = "warning" },
    WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
    PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
  },
  merge_keywords = true, -- when true, custom keywords will be merged with the defaults
  -- highlighting of the line containing the todo comment
  -- * before: highlights before the keyword (typically comment characters)
  -- * keyword: highlights of the keyword
  -- * after: highlights after the keyword (todo text)
  highlight = {
    before        = "", -- "fg" or "bg" or empty
    keyword       = "wide", -- "fg", "bg", "wide" or empty. (wide is the same as bg, but will also highlight surrounding characters)
    after         = "fg", -- "fg" or "bg" or empty
    pattern       = [[.*<(KEYWORDS)\s*:]], -- pattern or table of patterns, used for highlighting (vim regex)
    comments_only = true, -- uses treesitter to match keywords in comments only
    max_line_len  = 400, -- ignore lines longer than this
    exclude       = {}, -- list of file types to exclude highlighting
  },
  -- list of named colors where we try to extract the guifg from the
  -- list of hilight groups or use the hex color if hl not found as a fallback
  colors = {
	  error   = { "DiagnosticError", "ErrorMsg", "#DC2626" },
	  warning = { "DiagnosticWarning", "WarningMsg", "#FBBF24" },
	  info    = { "DiagnosticInfo", "#2563EB" },
	  hint    = { "DiagnosticHint", "#10B981" },
	  default = { "Identifier", "#7C3AED" },
  },
  search = {
    command = "rg",
    args = {
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
    },
    -- regex that will be used to match keywords.
    -- don't replace the (KEYWORDS) placeholder
    pattern = [[\b(KEYWORDS):]], -- ripgrep regex
    -- pattern = [[\b(KEYWORDS)\b]], -- match without the extra colon. You'll likely get false positives
  },
}
LUA

"""""""""""""""""""""""""""""""
"   OTHER GENERIC SETTINGS    "
"""""""""""""""""""""""""""""""
set nolist wrap linebreak breakat&vim
set number
set tabstop=4
set shiftwidth=4
set splitright

"""""""""""""""""""""""""""""""
"         SPELL CHECK         "
"""""""""""""""""""""""""""""""
" TODO: Move in colors file ?
hi clear SpellBad
hi SpellBad cterm=undercurl gui=undercurl guisp=red
hi clear SpellCap
hi SpellCap cterm=undercurl gui=undercurl guisp=yellow
hi clear SpellLocal
hi SpellLocal cterm=undercurl gui=undercurl guisp=white
hi clear SpellRare
hi SpellRare cterm=undercurl gui=undercurl guisp=cyan

"""""""""""""""""""""""""""""""
"     LANGUAGES SPECIFIC      "
"""""""""""""""""""""""""""""""
"Python
let g:python_highlight_all = 1
autocmd BufWritePost *.py call flake8#Flake8()

"CoC extensions (autocomplete, prettier, linting)
"let g:coc_global_extensions = ['coc-tslint-plugin', 'coc-tsserver', 'coc-css', 'coc-html', 'coc-json', 'coc-prettier']

"MARKDOWN
let g:vim_markdown_frontmatter = 1

autocmd bufnewfile *.md silent! so ~/.config/nvim/templates/markdown_header.txt
autocmd bufnewfile *.md silent! exe "1," . 10 . "g/title:.*/s//title: " .expand("%")
autocmd Bufwritepre,filewritepre *.md silent! execute "normal ma"
autocmd Bufwritepre,filewritepre *.md silent! exe "1," . 10 . "g/updated:.*/v/today/s/updated:.*/updated: " .strftime("%Y-%m-%d")
autocmd bufwritepost,filewritepost *.md silent! execute "normal `a"

"LATEX
let g:vimtex_view_general_viewer = 'zathura'

map K <Plug>latexfmt_format

augroup TEX
autocmd!
    autocmd FileType tex setlocal spelllang=fr,en_us spell wrap tw=80 wm=2
    autocmd BufWritePre *.tex %s/\s\+$//e
    " autocmd BufWritePost *.tex vsplit new | r! make
    " autocmd BufWritePost *.tex ! make
augroup END

augroup MD
    autocmd FileType markdown setlocal spelllang=fr,en_us spell wrap tw=80 wm=2
augroup END

augroup SKEL
  au!
  autocmd BufNewFile,BufRead *.sk   set syntax=skel
augroup END

" COC LANGUAGE SERVER
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

