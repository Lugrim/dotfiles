-- vim: set fileencoding=utf-8 :
-- vim: set filetype=lua :

conky.config = {
  use_spacer = 'left',
  pad_percents = 3,
  double_buffer = true,
  background = true,
  font = 'DejaVu Sans Mono:size=10',
  use_xft = true,
  override_utf8_locale = true,
  alignment = 'top_right',
  gap_x = 10,
  gap_y = 50,
  own_window_class = Conky,
  own_window_argb_visual = true,
  own_window_argb_value = 90,
  own_window_transparent = false,
  own_window_type = 'override',
  own_window_hints = 'undecorated,below,skip_taskbar,skip_pager,sticky',
  own_window = true,
  update_interval = 7.5,
}
conky.text = [[
#------------+
# Distro     |
#------------+
${color green}$alignc$nodename

# ${color}Today is:${color green}$alignr${time %A,}$alignr ${time %e %B %G}
${color}Distribution:${color green}$alignr ${execi 6300 neofetch --stdout | grep OS | cut -b 5- | rev | cut -b 2- | rev}
# $machine
${color}Host:${color green}$alignr ${execi 3600 neofetch --stdout | grep Host | cut -b 7- | rev | cut -b 2- | rev }
${color}Kernel:$alignr${color green} $kernel
${color orange}${voffset 2}${hr 1}
#------------+
# CPU        |
#------------+
${color2}${voffset 5}${execpi .001 (lscpu | grep 'Model name' | cut -b 22-)}${color1}  ${color green}${freq} MHz
# ${color}${goto 13}CPU 0 ${goto 81}${color green}${cpu cpu1}% ${goto 131}${color3}${cpubar cpu1 18}
# ${color}${goto 13}CPU 1 ${goto 81}${color green}${cpu cpu2}% ${goto 131}${color3}${cpubar cpu2 18}
${color3}${goto 13}${cpubar cpu1 18,100} ${cpubar cpu2 18,100} ${cpubar cpu3 18,100} ${cpubar cpu4 18,100}
${color3}${goto 13}${cpubar cpu5 18,100} ${cpubar cpu6 18,100} ${cpubar cpu7 18,100} ${cpubar cpu8 18,100}
${color3}${goto 13}${cpubar cpu9 18,100} ${cpubar cpu10 18,100} ${cpubar cpu11 18,100} ${cpubar cpu12 18,100}
${color3}${goto 13}${cpubar cpu13 18,100} ${cpubar cpu14 18,100}
#------------+
# Temperature|
#------------+
# Next line is for kernel >= 4.13.0-36-generic
${color1}All CPUs ${color green}${goto 81}${cpu}% ${goto 131}${color1}Temp: ${color green}${hwmon 2 temp 1}°C ${alignr}${color1}Up: ${color green}$uptime
# ${color1}Fan RPM: ${color green}${execpi 0.5 sensors | grep RPM | rev | cut -b 4- | rev | cut -b 14-} RPM
${color green}$running_processes ${color1}running of ${color green}$processes ${color1}loaded processes.
${color1}Load Average 1-5-15 minutes: ${alignr}${color green}${execpi .001 (awk '{printf "%s/", $1}' /proc/loadavg; grep -c processor /proc/cpuinfo;) | bc -l | cut -c1-4} ${execpi .001 (awk '{printf "%s/", $2}' /proc/loadavg; grep -c processor /proc/cpuinfo;) | bc -l | cut -c1-4} ${execpi .001 (awk '{printf "%s/", $3}' /proc/loadavg; grep -c processor /proc/cpuinfo;) | bc -l | cut -c1-4}
#------------+
# Intel iGPU |
#------------+
${if_existing /sys/class/drm/card0/gt_cur_freq_mhz}${color orange}${hr 1}
${color2}${voffset 5}${execi 9 glxinfo | grep 'Device:' | cut -b 13- | rev | cut -d ' ' -f 2- | rev}${alignr}${color green}${execpi .001 (cat /sys/class/drm/card0/gt_cur_freq_mhz)} MHz
${color}${goto 13}Min. Freq:${goto 120}${color green}${execpi .001 (cat /sys/class/drm/card0/gt_min_freq_mhz)} MHz${color}${goto 210}Max. Freq:${alignr}${color green}${execpi .001 (cat /sys/class/drm/card0/gt_max_freq_mhz)} MHz
# ${color orange}${hr 1}
${endif}
#------------+
# Nvidia GPU |
#------------+
${color orange}${hr 1}${if_match "${exec lsmod | grep nvidia_uvm}">"\n"}
${color2}${voffset 5}${execpi .001 (nvidia-smi --query-gpu=gpu_name --format=csv,noheader)} ${color1}@ ${color green}${execpi .001 (nvidia-smi --query-gpu=clocks.sm --format=csv,noheader)} ${alignr}${color1}Temp: ${color green}${execpi .001 (nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader)}°C
${color1}${voffset 5}Ver: ${color green}${execpi .001 (nvidia-smi --query-gpu=driver_version --format=csv,noheader)} ${color1} P-State: ${color green}${execpi .001 (nvidia-smi --query-gpu=pstate --format=csv,noheader)} ${alignr}${color1}BIOS: ${color green}${execpi .001 (nvidia-smi --query-gpu=vbios_version --format=csv,noheader)}
${color1}${voffset 5}GPU:${color green}${execpi .001 (nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader)} ${color1}Ram:${color green}${execpi .001 (nvidia-smi --query-gpu=utilization.memory --format=csv,noheader)} ${color1}Pwr:${color green}${execpi .001 (nvidia-smi --query-gpu=power.draw --format=csv,noheader)} ${alignr}${color1}Freq: ${color green}${execpi .001 (nvidia-smi --query-gpu=clocks.mem --format=csv,noheader)}
${color orange}${hr 1}
${endif}
#------------+
# Music      |
#------------+
# ${if_match "${execpi 15 mocp -i || echo 'sushi'}" != "sushi"}${color1}${voffset 5}Now playing: ${execi 3 mocp -i 2>/dev/null | grep "^Title: " | cut -b 8- | python3 -c "from sys import stdin; from os.path import basename, splitext; mastring=stdin.read(); print(splitext(basename(mastring.replace('  ', ' ')))[0], end = '') if mastring!='' else print('---');"}
# ${if_match "${execpi 15 mocp -i || echo 'sushi'}" != "sushi"}${color1}${voffset 5}Now playing: ${scroll 50 7 ${execi 3 mocp -i 2>/dev/null | grep "^Title: " | cut -b 8-}}
# ${color orange}${hr 1}
# ${endif}#
#------------+
# Processes  |
#------------+
${color1}${voffset 5}Process Name: ${goto 205}PID ${goto 285}CPU% ${goto 397}Mem ${alignr}Mem%
${color}${goto 13}${top_mem name 1} ${goto 190}${top_mem pid 1} ${goto 270}${color green}${top_mem cpu 1}${goto 365}${top_mem mem_res 1} ${alignr}${top_mem mem 1}
${color}${goto 13}${top_mem name 2} ${goto 190}${top_mem pid 2} ${goto 270}${color green}${top_mem cpu 2}${goto 365}${top_mem mem_res 2} ${alignr}${top_mem mem 2}
${color}${goto 13}${top_mem name 3} ${goto 190}${top_mem pid 3} ${goto 270}${color green}${top_mem cpu 3}${goto 365}${top_mem mem_res 3} ${alignr}${top_mem mem 3}
${color}${goto 13}${top_mem name 4} ${goto 190}${top_mem pid 4} ${goto 270}${color green}${top_mem cpu 4}${goto 365}${top_mem mem_res 4} ${alignr}${top_mem mem 4}
${color}${goto 13}${top_mem name 5} ${goto 190}${top_mem pid 5} ${goto 270}${color green}${top_mem cpu 5}${goto 365}${top_mem mem_res 5} ${alignr}${top_mem mem 5}
# ${color}${goto 13}${top_mem name 6} ${goto 190}${top_mem pid 6} ${goto 270}${color green}${top_mem cpu 6} ${alignr}${top_mem mem 6}
# ${color}${goto 13}${top_mem name 7} ${goto 190}${top_mem pid 7} ${goto 270}${color green}${top_mem cpu 7} ${alignr}${top_mem mem 7}
# ${color}${goto 13}${top_mem name 8} ${goto 190}${top_mem pid 8} ${goto 270}${color green}${top_mem cpu 8} ${alignr}${top_mem mem 8}
# ${color}${goto 13}${top_mem name 9} ${goto 190}${top_mem pid 9} ${goto 270}${color green}${top_mem cpu 9} ${alignr}${top_mem mem 9}
${color orange}${voffset 2}${hr 1}
#------------+
# Storage    |
#------------+
${color1}RAM Use/Free:${goto 148}${color red}$mem ${color red} ${goto 220}${membar 15,100} $alignr${color}${memeasyfree}
${color1}Linux Root:${goto 148}${color red}${fs_used /} ${color red} ${goto 220}${fs_bar 15,100 /} $alignr${color}${fs_free /}
${color1}Home:${goto 148}${if_mounted /home}${color red} ${fs_used /home} ${color red} ${goto 220}${fs_bar 15,100 /home} $alignr${color}${fs_free /home}${else} ${color yellow} /home ${endif}
# Static Storage
# ${color1}${if_mounted /media/limefox/StaticStorage}Static Storage:${goto 148}${color red} ${fs_used /media/limefox/StaticStorage} ${color red} ${goto 220}${fs_bar 15,100 /media/limefox/StaticStorage} $alignr${color}${fs_free /media/limefox/StaticStorage}${else} ${color yellow}<StaticStorage> ${endif}
# SDrive
# ${color1}${if_mounted /media/limefox/SDrive}SDrive:${goto 148}${color red}${fs_used /media/limefox/SDrive} ${color red} ${goto 220}${fs_bar 15,100 /media/limefox/SDrive} $alignr${color}${fs_free /media/limefox/SDrive}${else}${color yellow} <SDrive> ${endif}
# Footage
# ${color1}${if_mounted /media/limefox/Footage}Footage:${goto 148}${color red}${fs_used /media/limefox/Footage} ${color red} ${goto 220}${fs_bar 15,100 /media/limefox/Footage} $alignr${color}${fs_free /media/limefox/Footage}${else}${color yellow} <Footage> ${endif}
# Videos
# ${color1}${if_mounted /media/limefox/Videos}Videos:${goto 148}${color red}${fs_used /media/limefox/Videos} ${color red} ${goto 220}${fs_bar 15,100 /media/limefox/Videos} $alignr${color}${fs_free /media/limefox/Videos}${else}${color yellow} <Videos> ${endif}
# HDAnime
# ${color1}${if_mounted /media/limefox/HDAnime}HDAnime:${goto 148}${color red}${fs_used /media/limefox/HDAnime} ${color red} ${goto 220}${fs_bar 15,100 /media/limefox/HDAnime} $alignr${color}${fs_free /media/limefox/HDAnime}${else}${color yellow} <HDAnime> ${endif}
Swap:${goto 148}${color green}${swap} / ${swapmax} $alignr${color green}${swapperc}
${color orange}${voffset 2}${hr 1}
#-----------+
# RSS Feeds |
#-----------+
# ${rss https://bbs.archlinux.org/extern.php?action=feed&type=rss 1 item_titles 4}
# ${color white}${rss https://www.20minutes.fr/rss/une.xml 1 item_title 2}
# ${color 1}${rss https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/world/rss.xml 1 item_title 2}
# ${color 1}${weather }
# ${color orange}${voffset 2}${hr 1}
]]

