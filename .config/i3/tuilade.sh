#!/bin/sh

# i3-save-tree | sed "s/^\(\s*\)\/\/ \"/\1  \"/" | grep -v "^\s*//" | $HOME/.cargo/bin/tuilade -s | dot -Tpng | feh --class fehi3tree -
i3-msg -t get_tree | $HOME/.cargo/bin/tuilade -s -n | dot -Tpng | feh --class fehi3tree -
