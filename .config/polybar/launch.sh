#!/bin/bash

# Terminate already running bar instances
killall -q -- polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

source $HOME/.config/scripts/setup_env

N=${#MONITORS[@]}
M=$(((N-1)/2))

if [ "$N" -eq "1" ]; then
	MONITOR=${MONITORS} polybar single --config=~/.config/polybar/config.ini &
else
	MONITOR=${MONITORS[M]} polybar main --config=~/.config/polybar/config.ini &

	for i in $(seq 0 $((N-1)))
	do
		if [ "$i" -ne "$M" ]
		then
			MONITOR=${MONITORS[i]} polybar second --config=~/.config/polybar/config.ini &
		fi
	done
fi
# Launch Polybar, using default config location ~/.config/polybar/config
echo "$0: Polybar launched..."

